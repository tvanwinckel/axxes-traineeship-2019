package com.axxestraineeship.webmvc.exercise_2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/exercise-2")
public class BasicController {

    @GetMapping
    public String getBasicModelView(final Model model) {
        model.addAttribute("message", "Doing some Pre- and PostHandling. Might show something AfterCompletion.");
        return "basicView";
    }
}
