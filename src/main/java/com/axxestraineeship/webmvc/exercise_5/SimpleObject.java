package com.axxestraineeship.webmvc.exercise_5;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpleObject {

    private final String simpleString;
    private final int simpleInt;

    public SimpleObject(@JsonProperty(value = "simpleString") final String simpleString,
                        @JsonProperty(value = "simpleInt") final int simpleInt) {
        this.simpleString = simpleString;
        this.simpleInt = simpleInt;
    }

    public String getSimpleString() {
        return simpleString;
    }

    public int getSimpleInt() {
        return simpleInt;
    }

    @Override
    public String toString() {
        return "SimpleObject{" +
                "simpleString='" + simpleString + '\'' +
                ", simpleInt=" + simpleInt +
                '}';
    }
}
