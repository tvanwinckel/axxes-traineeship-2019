package com.axxestraineeship.webmvc.exercise_7;

import org.springframework.stereotype.Component;

import java.util.InputMismatchException;

@Component
public
class ExceptionService {

    public void nullPointer() {
        throw new NullPointerException();
    }

    public void indexOutOfBound() {
        throw new IndexOutOfBoundsException();
    }

    public void inputMismatch() {
        throw new InputMismatchException();
    }

    public void illegalArgument() {
        throw new IllegalArgumentException();
    }
}
