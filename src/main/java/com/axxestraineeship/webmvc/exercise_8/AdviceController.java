package com.axxestraineeship.webmvc.exercise_8;

import com.axxestraineeship.webmvc.exercise_7.ExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/exercise-8")
public class AdviceController {

    private final ExceptionService exceptionService;

    @Autowired
    public AdviceController(final ExceptionService exceptionService) {
        this.exceptionService = exceptionService;
    }

    @GetMapping(path = "/null")
    public String nullPointers(final Model model) {
        exceptionService.nullPointer();

        return setUpView(model);
    }

    @GetMapping(path = "/index")
    public String indexOutOfBound(final Model model) {
        exceptionService.indexOutOfBound();

        return setUpView(model);
    }

    @GetMapping(path = "/input")
    public String inputMismatch(final Model model) {
        exceptionService.inputMismatch();

        return setUpView(model);
    }

    @GetMapping(path = "/illegal")
    public String illegalArgument(final Model model) {
        exceptionService.illegalArgument();

        return setUpView(model);
    }

    private String setUpView(final Model model) {
        model.addAttribute("message", "If we get to see this message, something went wrong.");
        return "basicView";
    }
}
