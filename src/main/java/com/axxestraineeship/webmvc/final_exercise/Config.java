package com.axxestraineeship.webmvc.final_exercise;

import com.axxestraineeship.webmvc.final_exercise.domain.Consultant;
import com.axxestraineeship.webmvc.final_exercise.domain.Report;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class Config {

    @Bean
    public List<Report> reportDataset() {
        final List<Report> reports = new ArrayList<>();
        final Report report1 = new Report("12-ab-34", "Example report 1", "consultant-id", "This is an example reports summary");
        final Report report2 = new Report("34-cd-45", "Example report 2", "consultant-id", "This is an example reports summary");
        final Report report3 = new Report("45-ef-67", "Example report 3", "consultant-id", "This is an example reports summary");
        reports.add(report1);
        reports.add(report2);
        reports.add(report3);

        return reports;
    }

    @Bean
    public List<Consultant> consultantsDataset() {
        final List<Consultant> consultants = new ArrayList<>();
        consultants.add(new Consultant("consultant-id", "John", "Doe"));
        return consultants;
    }

}
