package com.axxestraineeship.webmvc.final_exercise.domain;

public class Consultant {

    private final String id;
    private final String firstName;
    private final String lastName;

    public Consultant(final String id, final String firstName, final String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public String fullName() {
        return firstName + " " + lastName;
    }
}
