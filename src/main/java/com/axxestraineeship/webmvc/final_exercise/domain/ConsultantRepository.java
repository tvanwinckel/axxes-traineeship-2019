package com.axxestraineeship.webmvc.final_exercise.domain;

public interface ConsultantRepository {

    Consultant findById(String consultantId);

    String findIdByFullName(String fullName);
}
