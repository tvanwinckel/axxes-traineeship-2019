package com.axxestraineeship.webmvc.final_exercise.domain;

import java.util.Objects;

public class Report {

    private final String id;
    private final String name;
    private final String authorId;
    private final String text;

    public Report(final String id, final String name, final String authorId, final String text) {
        this.id = id;
        this.name = name;
        this.authorId = authorId;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Report report = (Report) o;
        return Objects.equals(id, report.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
