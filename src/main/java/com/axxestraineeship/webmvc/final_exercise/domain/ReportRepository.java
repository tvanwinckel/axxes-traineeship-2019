package com.axxestraineeship.webmvc.final_exercise.domain;

import java.util.List;

public interface ReportRepository {

    List<Report> findAll();

    Report findById(String reportId);

    Report save(Report report);

    void delete(String reportId);
}
