package com.axxestraineeship.webmvc.final_exercise.infrastructure;

import com.axxestraineeship.webmvc.final_exercise.domain.Consultant;
import com.axxestraineeship.webmvc.final_exercise.domain.ConsultantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConsultantRepositoryImpl implements ConsultantRepository {

    private final List<Consultant> consultants;

    @Autowired
    public ConsultantRepositoryImpl(final List<Consultant> dataset) {
        this.consultants = dataset;
    }

    @Override
    public Consultant findById(String consultantId) {
        for (final Consultant c : consultants) {
            if (c.getId().equals(consultantId)) return c;
        }
        throw new ConsultantNotFoundException();
    }


    @Override
    public String findIdByFullName(final String fullName) {
        for (final Consultant c : consultants) {
            if (c.fullName().equals(fullName)) return c.getId();
        }
        throw new ConsultantNotFoundException();
    }

    class ConsultantNotFoundException extends RuntimeException {
    }
}
