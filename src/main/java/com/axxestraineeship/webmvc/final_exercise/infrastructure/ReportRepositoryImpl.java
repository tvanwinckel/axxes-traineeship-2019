package com.axxestraineeship.webmvc.final_exercise.infrastructure;

import com.axxestraineeship.webmvc.final_exercise.domain.Report;
import com.axxestraineeship.webmvc.final_exercise.domain.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportRepositoryImpl implements ReportRepository {

    private final Logger LOGGER = LoggerFactory.getLogger(ReportRepositoryImpl.class);
    private final List<Report> reports;

    @Autowired
    public ReportRepositoryImpl(final List<Report> dataset) {
        this.reports = dataset;
    }

    @Override
    public List<Report> findAll() {
        return reports;
    }

    @Override
    public Report findById(final String reportId) {
        for (final Report report : reports) {
            if (report.getId().equals(reportId)) return report;
        }
        throw new ReportNotFoundException();
    }

    @Override
    public Report save(final Report report) {
        return reports.contains(report)
                ? updateReport(report)
                : addReport(report);
    }

    @Override
    public void delete(String reportId) {
        reports.removeIf(r -> r.getId().equals(reportId));
    }

    private Report addReport(final Report report) {
        LOGGER.info("Saving new report with id: {}", report.getId());
        reports.add(report);
        return report;
    }

    private Report updateReport(final Report report) {
        LOGGER.info("Updating report with id: {}", report.getId());
        reports.remove(report);
        reports.add(report);
        return report;
    }

    class ReportNotFoundException extends RuntimeException {
    }
}
